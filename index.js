const {identity} = require('library-common/return');
const mysql = require('mysql');
const noop = require('library-common/noop');
const {orObj} = require('library-common/or');
const {throwIf} = require('library-common/throw');
const url = require('url');

// -----------------------------------------------------------------------------

module.exports = function Database(
	connectionString,
	{
		postConnection,
		preQuery,
		postQuery,
		errorQuery,
		poolOptions = {},
		noAutoConnection = false,
	}
) {
	throwIf(!connectionString, 'No database connection URL');

	const {hostname: host, port, pathname, auth} =
		url.parse(connectionString) || {};
	const [
		user,
		password,
	] = auth.split(':');

	const pool = mysql.createPool({
		...poolOptions,
		host,
		port,
		database: pathname.substr(1),
		user,
		password,
	});

	const config = {
		postConnection,
		preQuery,
		postQuery,
		errorQuery,
		pool,
		noAutoConnection,
	};

	return {
		query: query.bind(undefined, config),
		querySingle: querySingle.bind(undefined, config),
		connection: getConnection.bind(undefined, config),
		transaction: getTransaction,
		commit: commitTransaction,
		rollback: rollbackTransaction,
	};
};

// -----------------------------------------------------------------------------

async function query(config, queryString, params, existingConnection) {
	throwIf(
		config.noAutoConnection && !existingConnection,
		'No automatic connection!'
	);

	const connection = await getConnection(config, existingConnection);
	const results = await getResults(config, connection, queryString, params);

	if (!existingConnection) {
		connection.release();
	}

	return results;
}

async function querySingle(config, queryString, params, existingConnection) {
	const [result] = await query(
		config,
		queryString,
		params,
		existingConnection
	);

	return result;
}

// -----------------------------------------------------------------------------

function getConnection(
	{pool, postConnection = identity},
	existingConnection
) {
	return (
		existingConnection ||
		new Promise((resolve, reject) => {
			pool.getConnection((error, connection) => error ? reject(error) : resolve(postConnection(connection)));
		})
	);
}

function getResults(
	{preQuery = noop, postQuery = noop, errorQuery = identity},
	connection,
	queryString,
	params
) {
	return new Promise((resolve, reject) => {
		let queryState = {
			connection,
			query: queryString,
			params,
		};

		const databaseQuery = connection.query(
			{
				sql: queryString,
				typeCast: convertToJson,
			},
			params,
			(error, results) => {
				if (error) {
					errorQuery({
						...queryState,
						error,
					});
					reject(error);
				} else {
					postQuery({
						...queryState,
						results,
					});
					resolve(results);
				}
			}
		);

		//
		queryState.sql = databaseQuery.sql;
		queryState = {
			...queryState,
			...orObj(preQuery(queryState)),
		};
	});
}

function getTransaction(connection) {
	return new Promise((resolve, reject) => {
		connection.beginTransaction((error) => error ? reject(error) : resolve(connection));
	});
}

function commitTransaction(connection) {
	return new Promise((resolve, reject) => {
		connection.commit((error) => error ? reject(error) : resolve());
	});
}

function rollbackTransaction(connection) {
	return new Promise((resolve) => {
		connection.rollback(resolve);
	});
}

// -----------------------------------------------------------------------------

function convertToJson(field, next) {
	return field.type === 'JSON' ? JSON.parse(field.string()) : next();
}
